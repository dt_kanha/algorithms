#include <bits/stdc++.h>
#define working_days 5
#define num_periods 9

using namespace std;

string sequence = "PT";//For checking if its TP or PT sequence
string prac_first = "Not fixed"; //To check whether practical is in first half or second half on Wednesday
string tp_time_table[working_days+1][num_periods+1] = {{"TA", "TB", "TC", "TE", "LUNCH", "PA", "PA", "PA", "TF", "ZA"}, {"TB", "TC", "TD", "TE", "LUNCH", "PB", "PB", "PB", "TF", "ZB"}, {" ","PX", "PX", "PX", "LUNCH", "TG", "SA", "SB", "SC", "ZA"}, {"SM", "SJ", "SK", "SL", "LUNCH", " ", "PY", "PY", "PY", " "}, {"TC", "TD", "TA", "TG", "LUNCH", "PC", "PC", "PC", "TF", "ZC"}, {"TD", "TA", "TB", "TE", "LUNCH", "PD", "PD", "PD", "TG", "ZA"}};
string pt_time_table[working_days+1][num_periods+1] = {{"PE", "PE", "PE", "TE", "LUNCH", "TJ", "TK", "TL", "TF", "ZA"}, {"PF", "PF", "PF", "TE", "LUNCH", "TK", "TL", "TM", "TF", "ZB"}, {" ","PX", "PX", "PX", "LUNCH", "TG", "SA", "SB", "SC", "ZA"}, {"SM", "SJ", "SK", "SL", "LUNCH", " ", "PY", "PY", "PY", " "}, {"PG", "PG", "PG", "TG", "LUNCH", "TL", "TM", "TJ", "TF", "ZC"}, {"PH", "PH", "PH", "TE", "LUNCH", "TM", "TJ", "TK", "TG", "ZA"}};
string time_table[working_days][num_periods+1] = {{" "}};

struct subject{
	string subject_name;
	string subject_code;
	string time_slot;
	string tutorial_slot;
	int credits;
};
void update_student_time_table(subject *sub, string (&time_table_seq)[working_days+1][num_periods+1])
{
	int credits = sub->credits;
	string time_slot = sub->time_slot;
	string subject_code = sub->subject_code;
	string tutorial_slot = sub->tutorial_slot;
	int k = 0;//To keep track of time_table index
	int flag = 0;//To check if we got the tutorial slot for a 4 credit course in 3rd row i.e. i=2 itself or not if we did then we shall directly jump to thursday i.e. i=4 as per time_table_seq matrix not time_table matrix 
	if(credits<=3)
	{
		for(int i=0; i<working_days+1; i++)
		{
			if(i==3 && flag==1)//Means tutorial was found in i==2
			{
				continue;	
			}
			else if(i==3 && flag!=1)
			{
				k--;//Go back to index of wednesday and search again
			}
			for(int j=0; j<num_periods+1; j++)
			{
				if(j!=4)
				{
					if(i==2)//2nd row i.e. wednesday and 1st colum i.e. first subject of wednesday
					{
						if(j==0)
						{
							if(prac_first=="Not fixed")//Practical in first half or we don't have pracical slot on Wednesday check for theory in second half as its a 3 credit course
							{	
								j = j+4;//Search after lunch break
								continue;	
							}
							else if(prac_first=="True")//As there is no period in j==0 so skip it and move to j==1
							{
								continue;
							}
						}
						else
						{
							if(time_table_seq[i][j]=="PA" || time_table_seq[i][j]=="PB" || time_table_seq[i][j]=="PC" || time_table_seq[i][j]=="PD" || time_table_seq[i][j]=="PX" || time_table_seq[i][j]=="PE" || time_table_seq[i][j]=="PF" || time_table_seq[i][j]=="PG" || time_table_seq[i][j]=="PH" || time_table_seq[i][j]=="PY")
							{
								/*j = j+2;
								continue;*/
								if(time_table_seq[i][j]==time_slot)
								{	
									time_table[k][j] = subject_code;
									credits--;
									if(credits==0)
									{
										break;
									}	
								}
								else
								{
									j = j+2;
									continue;
								}
							}
							else
							{
								if(time_slot!="PA" && time_slot!="PB" && time_slot!="PC" && time_slot!="PD" && time_slot!="PX" && time_slot!="PE" && time_slot!="PF" && time_slot!="PG" && time_slot!="PH" && time_slot!="PY")
								{
									if(time_table_seq[i][j]==time_slot)
									{		
										time_table[k][j] = subject_code;
										credits--;
										if(credits==0)
										{
											break;
										}
										flag = 1;	
									}	
								}
								
//						else if(time_table_seq[i][j]==tutorial_slot)
//						{
//							string tutorial = subject_code+"_TA";
//							time_table[k][j] = tutorial;
//							credits--;
//							if(credits==0)
//							{
//								break;
//							}
//							
//						}
							}
						}
					}
					else
					{
						if(time_table_seq[i][j]=="PA" || time_table_seq[i][j]=="PB" || time_table_seq[i][j]=="PC" || time_table_seq[i][j]=="PD" || time_table_seq[i][j]=="PX" || time_table_seq[i][j]=="PE" || time_table_seq[i][j]=="PF" || time_table_seq[i][j]=="PG" || time_table_seq[i][j]=="PH" || time_table_seq[i][j]=="PY")
						{
							if(time_slot!="PA" && time_slot!="PB" && time_slot!="PC" && time_slot!="PD" && time_slot!="PX" && time_slot!="PE" && time_slot!="PF" && time_slot!="PG" && time_slot!="PH" && time_slot!="PY")
							{
								j = j+2;
								continue;
							}	
							else
							{
								if(time_table_seq[i][j]==time_slot)
								{
									time_table[k][j] = subject_code;
									time_table[k][j+1] = time_table[k][j];
									time_table[k][j+2] = time_table[k][j];
									credits = 0;
									break;	
								}
							}	
						}
						else
						{
							if(time_slot!="PA" && time_slot!="PB" && time_slot!="PC" && time_slot!="PD" && time_slot!="PX" && time_slot!="PE" && time_slot!="PF" && time_slot!="PG" && time_slot!="PH" && time_slot!="PY")
							{
								if(time_table_seq[i][j]==time_slot)
								{	
									time_table[k][j] = subject_code;
									credits--;
									if(credits==0)
									{
										break;
									}	
								}
							}	
						}	
					}
				}
				else
				{
					if(i==3 && (prac_first=="True"||prac_first=="Not fixed") && flag!=1)//Till lunch we couldn't find theory and after lunch there is practical PY slot so no chance of theory slot as per Time Table so directly go to next day and search there
					{
						break;
					}
					continue;
				}	
			}
			if(credits==0)
			{
				break;	
			}
			k++;	
		}	
	}
	else if(credits>3)
	{
		
		for(int i=0; i<working_days+1; i++)
		{
			if(i==3 && flag==1)//Means tutorial was found in i==2
			{
				continue;	
			}
			else if(i==3 && flag!=1)
			{
				k--;//Go back to index of wednesday and search again
			}
			for(int j=0; j<num_periods+1; j++)
			{
				if(j!=4)
				{
					if(i==2 && j==0)//2nd row i.e. wednesday and 1st colum i.e. first subject of wednesday
					{
						if(prac_first=="True" || prac_first=="Not fixed")//Practical in first half or we don't have pracical slot on Wednesday check for tutorial in second half
						{
							j = j+4;//Search after lunch break
							continue;	
						}
					}
					if(time_table_seq[i][j]=="PA" || time_table_seq[i][j]=="PB" || time_table_seq[i][j]=="PC" || time_table_seq[i][j]=="PD" || time_table_seq[i][j]=="PX" || time_table_seq[i][j]=="PE" || time_table_seq[i][j]=="PF" || time_table_seq[i][j]=="PG" || time_table_seq[i][j]=="PH" || time_table_seq[i][j]=="PY")
					{
						j = j+2;
						continue;
					}
					else
					{
						if(time_table_seq[i][j]==time_slot)
						{	
							time_table[k][j] = subject_code;
							credits--;
							if(credits==0)
							{
								break;
							}	
						}
						else if(time_table_seq[i][j]==tutorial_slot)
						{
							string tutorial = subject_code+"_TA";
							time_table[k][j] = tutorial;
							credits--;
							if(credits==0)
							{
								break;
							}
							flag = 1;
						}
					}
				}
				else
				{
					if(i==3 && flag!=1)//Till lunch we couldn't find tutorial and after lunch there is practical PY slot so no chance of tutorial as per Time Table so directly go to next day and search there
					{
						break;
					}
					continue;
				}	
			}
			if(credits==0)
			{
				break;	
			}
			k++;
		}	
	}	
}
void student_time_table(subject **subjects, int num)
{
	string w_days[5] = {"Mon", "Tue", "Wed", "Thu", "Fri"};//For working days
	for(int i=0; i<working_days; i++)
	{
		for(int j=0; j<num_periods+1; j++)
		{
			if(j==4)
			{
				time_table[i][j] = "LUNCH";		
			}
			else
			{
				time_table[i][j] = "N/A";
			}
		}
	}
	string time_table_seq[working_days+1][num_periods+1];
	if(sequence=="TP")
	{
		for(int i=0; i<working_days+1; i++)
		{
			for(int j=0; j<num_periods+1; j++)
			{
				time_table_seq[i][j] = tp_time_table[i][j];
			}
		}
	}
	else
	{
		for(int i=0; i<working_days+1; i++)
		{
			for(int j=0; j<num_periods+1; j++)
			{
				time_table_seq[i][j] = pt_time_table[i][j];
			}
		}
	}
	cout<<"Subject_Code"<<"   "<<"Subject_Name\n";
	for(int i=0; i<num; i++)
	{
		if(subjects[i]!=NULL)
		{
			cout<<subjects[i]->subject_code<<"          "<<subjects[i]->subject_name<<"\n";
			update_student_time_table(subjects[i], time_table_seq);
			cout<<"\n";	
		}
	}
	if(prac_first=="True")
	{
		time_table[2][0] = "N/A";
	}
	else if(prac_first=="False")
	{
		time_table[2][5] = "N/A";
		time_table[2][9] = "N/A";
	}
	cout<<"\t"<<"8-9AM"<<"   "<<"9-10AM"<<"   "<<"10-11AM"<<"   "<<"11AM-12PM"<<"   "<<"12-1:15PM"<<"   "<<"1:15-2:15PM"<<"   "<<"2:15-3:15PM"<<"   "<<"3:15-4:15PM"<<"   "<<"4:15-5:15PM"<<"   "<<"5:15-6:15PM"<<"\n";
	for(int i=0; i<working_days; i++)
	{
		cout<<w_days[i]<<"     ";
		for(int j=0; j<num_periods+1; j++)
		{
			if(j<=4)
			{
				if(time_table[i][j]=="N/A")
				{		
					cout<<"N/A"<<"       ";
				}
				else
				{
					cout<<time_table[i][j]<<"     ";	
				}	
			}
			else
			{
				if(time_table[i][j]=="N/A")
				{		
					cout<<"N/A"<<"          ";
				}
				else
				{
					cout<<time_table[i][j]<<"         ";	
				}
			}
		}
		cout<<"\n";
	}
}
void assign_tut_slot(subject *sub)
{
	string time_slot = sub->time_slot;
	if(time_slot=="TA")
	{
		sub->tutorial_slot = "SA";	
	}
	else if(time_slot=="TB")
	{
		sub->tutorial_slot = "SB";
	}
	else if(time_slot=="TC")
	{
		sub->tutorial_slot = "SC";
	}
	else if(time_slot=="TD")
	{
		sub->tutorial_slot = "TG";
	}
	else if(time_slot=="TE")
	{
		sub->tutorial_slot = "TG";
	}
	else if(time_slot=="TF")
	{
		sub->tutorial_slot = "TG";
	}
	else if(time_slot=="TJ")
	{
		sub->tutorial_slot = "SJ";
	}
	else if(time_slot=="TK")
	{
		sub->tutorial_slot = "SK";
	}
	else if(time_slot=="TL")
	{
		sub->tutorial_slot = "SL";
	}
	else if(time_slot=="TM")
	{
		sub->tutorial_slot = "SM";
	}
}
int main()
{
	int num;
	cout<<"-------------------------------------------------------NOTE-------------------------------------------------------------\n";
	cout<<"Time table subject to any specific time table specified by Dept. and meant to be used only for easy reference. App developers won't be held liable for any discrepancies in cases when a time table doesn't strictly follow central time table and subject to changes by any Dept.\n";
	cout<<"------------------------------------------------------WELCOME-----------------------------------------------------------\n";
	cout<<"Enter the number of subjects you are alloted: ";
	cin>>num;
	subject *subjects[num];
	for(int i=0; i<num; i++)
	{
		subjects[i] = NULL;
	}
	//cout<<"\nPlease provide details of subjects in following format: subject_name<space>credits<space>time_slot given in registration page of your NITRIS portal\n";
	//cout<<"Sample example: Data Communication"
	cout<<"\nPlease provide details of subjects as asked: NOTE:- Provide details as in your NITRIS registration portal. As an example you need to mention your time slot in Uppercase say TX as in your NITRIS portal and not as tx or Tx or anything else. In case of any other type or format of data the app would show error or not work properly:)";
	try{
		for(int i=0; i<num; i++)
		{
			subject *newSub = new subject; 
			cout<<"\nProvide details of subject "<<i+1<<":";
			cout<<"\nSubject Name: ";
			char ch = getchar();
			getline(cin, newSub->subject_name);
			cout<<"\n"<<newSub->subject_name<<" subject_code: ";
			cin>>newSub->subject_code;
			cout<<"\n"<<newSub->subject_name<<" time_slot: ";
			cin>>newSub->time_slot;
			for(int j=0; j<newSub->time_slot.size(); j++)
			{
				char ch = newSub->time_slot[j];
				if(islower(ch) || isdigit(ch))
				{
					cout<<"\nError!!! Please enter Time Slot in Capital letters as in NITRIS Portal and Try Again";
					return 1;
				}
			}
			if(newSub->time_slot=="PX")
			{
				prac_first = "True";
			}
			else if(newSub->time_slot=="PY")
			{
				prac_first = "False";
			}
			cout<<"\n"<<newSub->subject_name<<" credits: ";
			cin>>newSub->credits;
			subjects[i] = newSub;
			if(subjects[i]->time_slot=="PA" || subjects[i]->time_slot=="PB" || subjects[i]->time_slot=="PC" || subjects[i]->time_slot=="PD" || subjects[i]->time_slot=="PX")
			{
				sequence = "TP";
			}
			assign_tut_slot(subjects[i]);
		}	
		char ch = getchar();
		if(ch!='\n')
		{
			cout<<"\nError! Please provide correct details and try again\n";
		}
		else
		{
			cout<<"Success\n";
			cout<<"Your sequence is: "<<sequence<<"\n";
			cout<<"Your tentative time table is:\n";
			student_time_table(subjects, num);
			cout<<"NOTE: Time slots like XX which are not in central time table are not shown here. Consult your faculty advisor for more information about these slots:)\n";
		}	
	} catch(const exception &e){
		cout<<"Exception called due to "<<e.what()<<"\n";
		cout<<"Error!!! Try again\n";
	}	
	return 0;
}